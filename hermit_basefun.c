#include "basefun.h"

/*
	Wielomiany Hermite'a i jego pochodne.
*/


double fi(int i, double x)
{
	if(i == 0)
		return 1;
	else if (i==1)
		return 2 * x;
	else
		return 2 * x * fi(i-1, x) - 2 * (i-1) * fi(i-2, x); 
}

double dfi(int i, double x)
{
	if(i == 0)
		return 0;
	else if(i == 1)
		return 2;
	else
		return 2 * fi(i-1, x) + 2 * x * dfi(i-1, x) - 2 * (i-1) * dfi(i-2, x);
}

double d2fi(int i, double x)
{
	if(i < 2)
		return 0;
	else
		return 4 * dfi(i-1, x) + 2 * x * d2fi(i-1, x) - 2 * (i-1) * d2fi(i-2, x);
}
double d3fi(int i, double x)
{
	if(i < 2)
		return 0;
	else
		return 6 * d2fi(i-1, x) + 2 * x * d3fi(i-1, x) - 2 * (i-1) * d3fi(i-2, x);
}

