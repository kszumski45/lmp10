#ifndef BASEFUN
#define BASEFUN

double fi(int i, double x);
double dfi(int i, double x);
double d2fi(int i, double x);
double d3fi(int i, double x);

#endif
