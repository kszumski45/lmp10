#include "makespl.h"
#include "piv_ge_solver.h"
#include "basefun.h"
#include <float.h>

/*
	Aproksymator średniokwadratowy wykorzystujący 
	funkcje bazowe zdefiniowane w basefun.h
	zwracający wynik w formie funkcji sklejanych
*/

#define PZ 50 /*liczba przedziałów funkcji sklejanej*/
#define LFB 20 /*rząd wielomianu użyty do aproksymacji*/


static int solve(matrix_t *eqs, points_t *pts, int a, int b, int lfb)
{
	
	double *x = pts->x;
	double *y = pts->y;
		
	
	int i, j, k;
	for(j = 0; j < lfb; j++){
		for(i = 0; i < lfb; i++)
			for(k = a; k < b; k++)
			{
				add_to_entry_matrix(eqs, j, i, fi(i, x[k]) * fi(j, x[k]));
			}
		for(k = a; k < b; k++)
			add_to_entry_matrix(eqs, j, lfb, y[k] * fi(j, x[k]));			

		fprintf(stderr,"Generowanie układu równań: (%d : %d)\n", j*lfb + i  , lfb * lfb  );
	}

/* Rozwiązanie */

	printf("Rozwiązywanie układu równań\n");
	return piv_ge_solver(eqs);


}

void make_spl(points_t *pts, spline_t *spl)
{

	double *x = pts->x;
	double *y = pts->y;
	
	double a = x[0];
	double b = x[pts->n -1];
	int pz = PZ; /* liczba przedziałów */	
	matrix_t *eqs = NULL;

	int lfb = pts->n -3 > 10 ? LFB : pts->n -3; /* liczba funkcji bazowych */
 	eqs = make_matrix(lfb, lfb + 1);		

	if(pz < lfb)  /*użycie mniejszej liczby przedziałów niż rząd funkcji aproksymującej generuje nieciągły wykres*/
		pz = lfb;
	
/* Wygenerowanie spline'u na podstawie współczynników otrzymanych
	z rozwiązania układu równań
 */

	if(solve(eqs, pts, 0, pts->n, lfb))	
	{
		spl-> n = 0;
		return;
	}
	int i, j, k;
	if(alloc_spl(spl, pz) == 0) {
		for(i = 0; i < spl->n; i++) {

			double xx = spl->x[i] = a + i *(b-a)/(spl->n-1);
			xx += 10.0*DBL_EPSILON;

			spl->f[i] = 0;
			spl->f1[i] = 0;
			spl->f2[i] = 0;
			spl->f3[i] = 0;

			for(k = 0; k < lfb; k++) {
				double ck = get_entry_matrix(eqs, k ,lfb);
				spl->f[i]  += ck * fi  (k, xx);
				spl->f1[i] += ck * dfi (k, xx);
				spl->f2[i] += ck * d2fi(k, xx);
				spl->f3[i] += ck * d3fi(k, xx);
			}
			
         fprintf(stderr, "Generownane funkcji sklejanych: (%d : %d)\n", i *lfb + k , spl->n * lfb);
		}
	}	
}

