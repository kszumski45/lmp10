aprox: main.o splines.o points.o aproksymator_na_bazie.o gaus/libge.a
	$(CC) -o aprox  main.o splines.o points.o aproksymator_na_bazie.o -L gaus -l ge

aprox_hermit: main.o splines.o points.o aprok_srednio.o hermit_basefun.o gaus/libge.a
	$(CC) -o aprox_hermit main.o splines.o points.o aprok_srednio.o hermit_basefun.o -L gaus -l ge

intrp: main.o splines.o points.o interpolator.o gaus/libge.a
	$(CC) -o intrp  main.o splines.o points.o interpolator.o -L gaus -l ge

prosta: main.o splines.o points.o prosta.o
	$(CC) -o prosta  main.o splines.o points.o prosta.o	

aproksymator_na_bazie.o: makespl.h points.h gaus/piv_ge_solver.h
	$(CC) -I gaus -c aproksymator_na_bazie.c

aprok_srednio.o: makespl.h points.h basefun.h gaus/piv_ge_solver.h
	$(CC) -I gaus -c aprok_srednio.c

interpolator.o: makespl.h points.h gaus/piv_ge_solver.h
	$(CC) -I gaus -c interpolator.c

hermit_basefun.o: basefun.h
	$(CC) -c hermit_basefun.c

.PHONY: clean

clean:
	-rm *.o aprox aprox_hermit intrp prosta
